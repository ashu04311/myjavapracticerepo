package com.question.ds;

public class CustomLinkedList<T> {
	private Node<T> head;
	private Node<T> tail;

	public CustomLinkedList() {
	}

	public void add(T value) {
		if (head == null) {
			head = new Node<T>();
			head.setValue(value);
			tail = head;
		} else {
			Node<T> node = new Node<T>();
			node.setValue(value);
			tail.setNext(node);
			tail = node;
		}
	}

	public void remove(T value) {
		if (head.getValue() == value) {
			head = head.getNext();
			return;
		}
		Node<T> node = head;
		while (node != null) {
			if (node.getNext().getValue() == value) {
				node.setNext(node.getNext().getNext());
				break;
			} else {
				node = node.getNext();
			}
		}
	}
	
	public void addAfter(T value , T newValue) {
		Node<T> node = head;
		while(node != null) {
			if(node.getValue() == value) {
				Node<T> newNode = new Node<T>();
				newNode.setValue(newValue);
				newNode.setNext(node.getNext());
				node.setNext(newNode);
				return;
			} else {
				node = node.getNext();
			}
		}
		System.out.println(value +" is not present");
	}
	
	public void addBefore(T value, T newValue) {
		
	}

	public void printList() {
		Node<T> node = head;
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		while (node != null) {
			sb.append(node.getValue());
			node = node.getNext();
			if(node != null)
				sb.append(" ,");
		}
		sb.append("]");
		System.out.println(sb);
	}

	class Node<T> {
		private Node<T> next;
		private T value;

		public Node<T> getNext() {
			return next;
		}

		public void setNext(Node<T> next) {
			this.next = next;
		}

		public T getValue() {
			return value;
		}

		public void setValue(T value) {
			this.value = value;
		}

	}

	public static void main(String[] args) {
		CustomLinkedList<Integer> list = new CustomLinkedList<Integer>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(6);
		list.add(7);
		list.printList();
		list.addAfter(6, 99);
		list.addAfter(2, 66);
		list.printList();
	}

}
